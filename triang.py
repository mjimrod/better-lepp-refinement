# Autor Mitchel Jimenez
import numpy as np
import matplotlib.pyplot as plt
from time import sleep
import csv

# Clase Triangle: son los triangulos. Cada triangulo tiene asociado 3 vertices
# que lo conforman y ademas los 3 triangulos vecinos posible que tiene.
# Ademas, le colocamos un indice que sera su posicion en el arreglo de la Clase
# Triangulation (esto para no tener que hacer busquedas todo el tiempo al
# remover triangulos de la triangulacion - esto ultimo ocurre cuando insertamos
# un nuevo vertice -).
class Triangle:
    def __init__(self, vertexAssociated = [], nearTriangles = []):
        self.vertexAssociated = vertexAssociated # Lista de vertices que lo conforman
        self.nearTriangles = nearTriangles # Lista de triangulos vecinos
        self.index = 0 # Indice en el arreglo de la clase Triangulation

    # Con esta funcion recibimos un triangulo y dos vertices que pertenecen a
    # ese triangulo y encontramos el indice restante del triangulo
    def findOtherVertex(self, vertex1, vertex2):
        for i in range(3):
            v = self.vertexAssociated[i]
            if (not v.isEqual(vertex1)) and (not v.isEqual(vertex2)):
                return v
        # Esto no deberia pasar puesto que no deberian crearse triangulos
        # con 2 o 3 puntos iguales, que de por si, no son triangulos,
        # siempre deberia encontrarse algun v que cumpla con tal condicion

        return None

    # Esta funcion nos entrega el indice en la lista vertexAssociated que tiene
    # un cierto vertice. Retorna None si el vertice no esta en ese triangulo
    def giveMeIndex(self, vertex):
        for i in range(3):
            if vertex.isEqual(self.vertexAssociated[i]):
                return i
        return None

    # Esta funcion encuentra la arista mas larga y retorna la misma arista
    # con el indice del vertice al que esta opuesta, esto para poder navegar
    # mas facilmente al triangulo vecino
    def longestEdge(self):
        # Obtengo mis puntos
        a = np.array([self.vertexAssociated[0].x, self.vertexAssociated[0].y])
        b = np.array([self.vertexAssociated[1].x, self.vertexAssociated[1].y])
        c = np.array([self.vertexAssociated[2].x, self.vertexAssociated[2].y])

        # Vectores
        ab = b - a
        bc = c - b
        ca = a - c

        # Calculo las normas
        longAB = np.linalg.norm(ab)
        longBC = np.linalg.norm(bc)
        longCA = np.linalg.norm(ca)

        # Calculo el maximo
        maximo = max(longAB, longBC, longCA)

        # Decido y entrego
        if maximo == longAB:
            return Edge([self.vertexAssociated[0], self.vertexAssociated[1]]), 2
        elif maximo == longBC:
            return Edge([self.vertexAssociated[1], self.vertexAssociated[2]]), 0
        else:
            return Edge([self.vertexAssociated[2], self.vertexAssociated[0]]), 1

        return None

    # Funcion que verifica si un triangulo es bueno
    def isBad(self):
        # Angulo minimo
        minTheta = np.pi/6

        # Tengo los vertices
        a = np.array([float(self.vertexAssociated[0].x), float(self.vertexAssociated[0].y)])
        b = np.array([float(self.vertexAssociated[1].x), float(self.vertexAssociated[1].y)])
        c = np.array([float(self.vertexAssociated[2].x), float(self.vertexAssociated[2].y)])

        # Tengo los vectores
        ab = b - a
        bc = c - b
        ca = a - c

        longAB = np.linalg.norm(ab)
        longBC = np.linalg.norm(bc)
        longCA = np.linalg.norm(ca)

        # Tengo los angulos
        theta = [np.arccos((longCA**2 + longBC**2 - longAB**2)/(2 * longCA * longBC)),
                  np.arccos((longAB**2 + longBC**2 - longCA**2)/(2 * longAB * longBC)),
                  np.arccos((longAB**2 + longCA**2 - longBC**2)/(2 * longAB * longCA))]
        if min(theta) < minTheta:
            return True
        return False

    # Calcula su area
    def area(self):
        # Vertices
        a = np.array([float(self.vertexAssociated[0].x), float(self.vertexAssociated[0].y)])
        b = np.array([float(self.vertexAssociated[1].x), float(self.vertexAssociated[1].y)])
        c = np.array([float(self.vertexAssociated[2].x), float(self.vertexAssociated[2].y)])

        # Calculo dos de sus vectores
        ab = b - a
        bc = c - b

        # Calculo su producto cruz
        cross = np.cross(ab, bc)

        # Y su norma
        norm = np.linalg.norm(cross)

        # Retorno el area
        return (float(1)/2) * norm

    # Calcula su centroide
    def centroid(self):
        # Vertices
        a = np.array([float(self.vertexAssociated[0].x), float(self.vertexAssociated[0].y)])
        b = np.array([float(self.vertexAssociated[1].x), float(self.vertexAssociated[1].y)])
        c = np.array([float(self.vertexAssociated[2].x), float(self.vertexAssociated[2].y)])

        # Retorno el centroide como un np.array
        return (a + b + c) / 3

    # Printea la informacion del triangulo
    def printPlease(self):
        print("Triangulo indice " + str(self.index) + ": ",
            [self.vertexAssociated[0].x, self.vertexAssociated[0].y], " ",
            [self.vertexAssociated[1].x, self.vertexAssociated[1].y], " ",
            [self.vertexAssociated[2].x, self.vertexAssociated[2].y])
        return None

# Clase Vertex: son los vertices. Tienen dos valores reales asociados a la pos.
class Vertex:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Esta funcion entrega un numero positivo si el punto c se encuentra a la
    # izquierda de la arista formada entre a y b. Entrega un numero negativo si
    # se encuentra a la derecha de la arista y cero si son puntos colineales.
    # Basicamente da la orientacion de los tres puntos.
    def orient2d(self, a, b):
        matrix = np.array([[a.x - self.x, a.y - self.y],
                           [b.x - self.x, b.y - self.y]]
                           , dtype = np.float64)
        # return np.linalg.det(matrix)
        r = np.linalg.det(matrix)
        if r > 0 and r < 1e-15:
            return 0
        return r

    # Esta funcion verifica si un vertice es igual a otro
    def isEqual(self, vertex):
        if self.x == vertex.x and self.y == vertex.y:
            return True
        return False

    # Metodo que verifique si es colineal y entregue la arista con la cual es
    # colineal o si el vertice es un vertice repetido, retorna el vertice.
    # La idea es que este metodo se llame dentro de isInside para saber si es
    # colineal
    def isCollinear(self, triangle):
        # si es un vertice repetido
        for i in range(3):
            if self.isEqual(triangle.vertexAssociated[i]):
                return self

        # si es colineal a alguna arista
        for i in range(3):
            if self.orient2d(triangle.vertexAssociated[i], triangle.vertexAssociated[(i + 1) % 3]) == 0:
                return [i, (i + 1) % 3]
        # Cuando no es ni vertice repetido ni colineal con ninguna arista
        # y solo esta dentro del triangulo, retornamos True, sin embargo,
        # este True corresponde a la respuesta del isInside (recordemos que
        # isCollinear es una funcion auxiliar de isInside).
        return True

    # Metodo que verifica si un vertice esta dentro de un triangulo
    def isInside(self, triangle):
        assert type(triangle) == Triangle or triangle == None
        if triangle == None:
            return False

        # Verifica que el punto este siempre a la izquierda de cada arista
        # del triangulo.
        result = []
        for i in range(3):
            # Si es un vertice repetido
            if self.isEqual(triangle.vertexAssociated[i]):
                return self
            result.append(self.orient2d(triangle.vertexAssociated[i], triangle.vertexAssociated[(i + 1) % 3]))
        # Si esta a la izquierda de todas las aristas
        if result[0] > 0 and result[1] > 0 and result[2] > 0:
            return True
        # Si es colineal retorno la arista
        if result[0] == 0:
            if result[1] > 0 and result[2] > 0:
                return [0, 1]
        elif result[1] == 0:
            if result[2] > 0 and result[0] > 0:
                return [1, 2]
        elif result[2] == 0:
            if result[0] > 0 and result[1] > 0:
                return [2, 0]

        # Pues es falso, no esta dentro de nada
        return False

    # Compara dos vertices para saber si son iguales
    def isEqualv(self, vertex):
        if self.x == vertex.x and self.y == vertex.y:
            return True
        return False

    # Revisa si un punto esta dentro del circulo, esta sobre el circulo o fuera
    def inCircle(self, triangle):
        assert type(triangle) == Triangle

        # Vertices a usar
        a = triangle.vertexAssociated[0]
        b = triangle.vertexAssociated[1]
        c = triangle.vertexAssociated[2]

        # Matriz a calcularle el determinante
        matrix = np.array([[a.x, a.y, a.x**2 + a.y**2, 1],
                           [b.x, b.y, b.x**2 + b.y**2, 1],
                           [c.x, c.y, c.x**2 + c.y**2, 1],
                           [self.x, self.y, self.x**2 + self.y**2, 1]])
        determinante = np.linalg.det(matrix)
        if determinante > 0:
            return True
        return False

    # Busca el vertice en dos triangulos y decide si se encuentra contenido o
    # no en uno de ellos (el otro modo es estar sobre una arista)
    def searchVertexInTwoTriangles(self, t1, t2):
        assert type(t1) == Triangle
        assert type(t2) == Triangle

        # Probamos que este en t1
        ans = self.isInside(t1)
        if type(ans) == list:
            return ans, t1
        else: # Si no esta en t1, pues debe estar en t2
            return True, t2


# Clase Arista: hace mas facil el trabajo con las aristas.
class Edge:
    def __init__(self, vertices = []):
        self.vertices = vertices
        self.constrained = False

    # Compruebo si dos aristas son iguales
    def isEqual(self, e):
        assert type(e) == Edge

        # Compruebo que vertices de las aristas sean iguales
        if (self.vertices[0].isEqualv(e.vertices[0]) and self.vertices[1].isEqualv(e.vertices[1])) ^ (self.vertices[0].isEqualv(e.vertices[1]) and self.vertices[1].isEqualv(e.vertices[0])):
            return True
        # Si no, retorno false
        return False

    # Calcula el punto medio de un Edge y retorna el vertice correspondiente
    def middle(self):
        return Vertex((self.vertices[0].x + self.vertices[1].x)/2,
                (self.vertices[0].y + self.vertices[1].y)/2)

# Clase de la triangulacion: es basicamente un arreglo de triangulos y un
# arreglo de vertices, ademas de las coordenada maxima permitida,
# un contador de triangulos malos, un string para ayudarnos con el modo
# interactivo y, finalmente, un arreglo de aristas restringidas
class Triangulation:
    def __init__(self, maxCoord = 1000):
        self.vertexList = []
        self.triangleList = []
        self.edgesConstrained = []
        self.maxCoord = maxCoord
        self.interactive = ""
        self.badTriangles = 1

    # Printea todos los triangulos
    def printTriangles(self):
        print("\n___________________________________________________")
        print("\nLista de triangulos de la triang.:\n")
        for i in range(len(self.triangleList)):
            triangle = self.triangleList[i]
            if triangle == None:
                print("Triangulo " + str(i) + ": None")
                continue
            print("Triangulo " + str(i) + ": ",
                [triangle.vertexAssociated[0].x, triangle.vertexAssociated[0].y], " ",
                [triangle.vertexAssociated[1].x, triangle.vertexAssociated[1].y], " ",
                [triangle.vertexAssociated[2].x, triangle.vertexAssociated[2].y])

    # Con esto buscamos en que triangulo esta contenido nuestro punto y
    # retornamos lo que retorna la funcion isCollinear para identificar en donde
    # cayo el punto sobre el triangulo en el que esta el vertice contenido,
    def searchVertex(self, vertex):
        assert type(vertex) == Vertex

        # Iteramos sobre el arreglo de triangulos
        for i in range(len(self.triangleList)):
            if self.triangleList[i] == None:
                continue
            ans = vertex.isInside(self.triangleList[i])
            if ans != False:
                return ans, self.triangleList[i]

        # Si no encontramos ningun triangulo donde el vertice este dentro
        # retornamos el triangulo None, esto sin embargo no deberia pasar nunca
        # porque no deberia haber ningun punto fuera de nuestro cuadrado hecho
        # con las coordenadas infinitas
        return None

    # Con esto creamos un triangulo y lo anadimos a nuestra lista de triangulos
    def createTriangle(self, vertexAssociated):
        triangle = Triangle(vertexAssociated)
        # Le asignamos como indice el largo de la lista triangleList
        # Esto funciona pues la lista parte en cero.
        triangle.index = len(self.triangleList)
        self.triangleList.append(triangle)

        if self.interactive == 'y':
            # Dibujamos interactivamente el triangulo creado
            xdata = []
            ydata = []
            line, = ax.plot(xdata, ydata, 'k')
            for j in range(4):
                v = triangle.vertexAssociated[j % 3]
                xdata.append(v.x)
                ydata.append(v.y)
                plt.scatter(v.x, v.y)
                line.set_xdata(xdata)
                line.set_ydata(ydata)
                plt.draw()
                plt.pause(1e-17)
                # Descomentar para hacer el dibujo mas lento, tambien podemos
                # aumentar el 0.1 para hacerlo mas lento aun
                # sleep(0.1)

        return triangle

    # Con esta funcion creamos el cuadrado sobre toda nuestra triangulacion
    def firstQuad(self):
        # Encontramos las coordenadas limite
        infCoords = [0, 0, self.maxCoord, self.maxCoord]

        # Creamos cuatro vertices que forman el triangulo usando las coordenadas
        # "infinitas" y los anadimos a nuestra lista de vertices.
        dx = 1
        dy = 1

        # Agregamos los vertices del cuadrado
        self.vertexList.insert(0, Vertex(infCoords[0] - dx, infCoords[1] - dy))
        self.vertexList.insert(1, Vertex(infCoords[2] + dx, infCoords[1] - dy))
        self.vertexList.insert(2, Vertex(infCoords[2] + dx, infCoords[3] + dy))
        self.vertexList.insert(3, Vertex(infCoords[0] - dx, infCoords[3] + dy))

        # Inicializamos nuestros ejes de dibujo interactivo, comentar si se
        # quiere desactivar el dibujo interactivo
        if self.interactive == "y":
            # Setea el axis de matplotlib
            ax.set(title="Simple Interactive Triangulation", xlim=[self.vertexList[0].x, self.vertexList[1].x],
                ylim=[self.vertexList[0].y, self.vertexList[2].y])

        # Creamos una lista de vertices que conforman cada triangulo inicial
        vertexListOne = [self.vertexList[0], self.vertexList[1], self.vertexList[2]]
        vertexListTwo = [self.vertexList[2], self.vertexList[3], self.vertexList[0]]

        # Creamos nuestro cuadrado base
        self.createTriangle(vertexListOne)
        self.createTriangle(vertexListTwo)

        # Actualizamos sus listas nearTriangles (triangulos vecinos)
        self.triangleList[0].nearTriangles = [None, self.triangleList[1], None]
        self.triangleList[1].nearTriangles = [None, self.triangleList[0], None]

        return None

    # Con esto creamos la triangulacion entera, esto nos deberia crear todos
    # los triangulos en nuestra triangulacion y terminarla basicamente
    # Sin embargo, esta funcion solo sirve de controlador y Master Puppet
    # de los demas metodos.
    def createTriangulation(self, vertexToAdd):
        # Creamos los primeros dos triangulos
        self.firstQuad()
        i = 4
        # Empezamos a recorrer los vertices que vamos anadiendo a nuestra
        # triangulacion
        while i < len(vertexToAdd):
            # Guardamos la informacion que nos de searchVertex
            isInside_CollinearOrDuplicated, triangleFound = self.searchVertex(vertexToAdd[i])
            # Si el vertice no estaba contenido en el triangulo, sino que
            # estaba sobre una arista o es un vertice repetido
            if isInside_CollinearOrDuplicated != True:
                # Si es un vertice repetido, no hago nada
                if type(isInside_CollinearOrDuplicated) == Vertex:
                    i += 1
                    continue
                # Si el vertice estaba sobre una arista
                else:
                    # Una expresion matematica para calcular el indice del vertice restante
                    # de un triangulo dados los otros dos indices de vertices
                    indexFaltante = (3 -((isInside_CollinearOrDuplicated[1] +
                        isInside_CollinearOrDuplicated[0]) % 3)) % 3

                    # 4 vertices a usar
                    cero = triangleFound.vertexAssociated[isInside_CollinearOrDuplicated[1]]
                    uno = triangleFound.vertexAssociated[indexFaltante]
                    dos = triangleFound.vertexAssociated[isInside_CollinearOrDuplicated[0]]

                    neighborTriangle = triangleFound.nearTriangles[indexFaltante]

                    # Necesitamos el 4to vertice a usar que esta en el triangulo
                    # contrario al vertice indexFaltante
                    tres = neighborTriangle.findOtherVertex(cero, dos)

                    # Creamos los triangulos nuevos
                    D = self.createTriangle([cero, uno, vertexToAdd[i]])
                    C = self.createTriangle([vertexToAdd[i], uno, dos])
                    F = self.createTriangle([dos, tres, vertexToAdd[i]])
                    E = self.createTriangle([vertexToAdd[i], tres, cero])

                    # Triangulos vecinos
                    c0 = triangleFound.nearTriangles[triangleFound.giveMeIndex(cero)]
                    c1 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(cero)]
                    c2 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(dos)]
                    c3 = triangleFound.nearTriangles[triangleFound.giveMeIndex(dos)]

                    # Les actualizamos la lista de triangulos vecinos
                    D.nearTriangles = [C, E, c3]
                    C.nearTriangles = [c0, F, D]
                    F.nearTriangles = [E, C, c1]
                    E.nearTriangles = [c2, D, F]

                    # Y a los vecinos tambien les actualizamos las listas
                    if c0 != None:
                        c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(uno, dos))] = C
                    if c1 != None:
                        c1.nearTriangles[c1.giveMeIndex(c1.findOtherVertex(dos, tres))] = F
                    if c2 != None:
                        c2.nearTriangles[c2.giveMeIndex(c2.findOtherVertex(tres, cero))] = E
                    if c3 != None:
                        c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(cero, uno))] = D

                    # Matamos el triangulo encontrado y el vecino,
                    # reemplazandolo por None en la lista
                    self.killTriangle(triangleFound)
                    self.killTriangle(neighborTriangle)
                    # Continuamos al siguiente ciclo
                    i += 1
                    continue

            # Si es isInsideCollinearOrDuplicated es True, entonces el vertice
            # buscado estaba dentro de un triangulo, asi que hay que crear 3
            # triangulos nuevos sobre el encontrado.

            # 3 vertices a usar y tres vecinos
            cero = triangleFound.vertexAssociated[0]
            uno = triangleFound.vertexAssociated[1]
            dos = triangleFound.vertexAssociated[2]
            n0 = triangleFound.nearTriangles[2]
            n1 = triangleFound.nearTriangles[0]
            n2 =  triangleFound.nearTriangles[1]

            # Creamos los nuevos triangulos
            D = self.createTriangle([cero, uno, vertexToAdd[i]])
            A = self.createTriangle([vertexToAdd[i], uno, dos])
            B = self.createTriangle([dos, cero, vertexToAdd[i]])

            # Actualizamos la lista de triangulos vecinos de cada uno
            D.nearTriangles = [A, B, n0]
            A.nearTriangles = [n1, B, D]
            B.nearTriangles = [D, A, n2]

            # Y a los vecinos tambien se las actualizamos
            if n0 != None:
                n0.nearTriangles[n0.giveMeIndex(n0.findOtherVertex(cero, uno))] = D
            if n1 != None:
                n1.nearTriangles[n1.giveMeIndex(n1.findOtherVertex(uno, dos))] = A
            if n2 != None:
                n2.nearTriangles[n2.giveMeIndex(n2.findOtherVertex(dos, cero))] = B

            # Matamos el triangulo encontrado, reemplazandolo por None en la
            # lista de triangulos de la triangulacion
            self.killTriangle(triangleFound)
            i += 1
        return None

    # Esto es anhadir los puntos de forma interactiva
    def addVertex(self, vertexToAdd):
        # Guardamos la informacion que nos de searchVertex
        isInside_CollinearOrDuplicated, triangleFound = self.searchVertex(vertexToAdd)
        # Si el vertice no estaba contenido en el triangulo, sino que
        # estaba sobre una arista o es un vertice repetido
        if isInside_CollinearOrDuplicated != True:
            # Si es un vertice repetido, no hago nada
            if type(isInside_CollinearOrDuplicated) == Vertex:
                return
            # Si el vertice estaba sobre una arista
            else:
                # Lo agrego a la lista
                self.vertexList.append(vertexToAdd)

                # Una expresion matematica para calcular el indice del vertice restante
                # de un triangulo dados los otros dos indices de vertices
                indexFaltante = (3 -((isInside_CollinearOrDuplicated[1] +
                    isInside_CollinearOrDuplicated[0]) % 3)) % 3

                # 3 vertices a usar
                cero = triangleFound.vertexAssociated[isInside_CollinearOrDuplicated[1]]
                uno = triangleFound.vertexAssociated[indexFaltante]
                dos = triangleFound.vertexAssociated[isInside_CollinearOrDuplicated[0]]

                # Triangulo vecino
                neighborTriangle = triangleFound.nearTriangles[indexFaltante]

                # Necesitamos el 4to vertice a usar que esta en el triangulo
                # contrario al vertice indexFaltante
                tres = neighborTriangle.findOtherVertex(cero, dos)

                # Creamos los triangulos nuevos
                D = self.createTriangle([cero, uno, vertexToAdd])
                C = self.createTriangle([vertexToAdd, uno, dos])
                F = self.createTriangle([dos, tres, vertexToAdd])
                E = self.createTriangle([vertexToAdd, tres, cero])

                # Triangulos vecinos
                c0 = triangleFound.nearTriangles[triangleFound.giveMeIndex(cero)]
                c1 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(cero)]
                c2 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(dos)]
                c3 = triangleFound.nearTriangles[triangleFound.giveMeIndex(dos)]

                # Les actualizamos la lista de triangulos vecinos
                D.nearTriangles = [C, E, c3]
                C.nearTriangles = [c0, F, D]
                F.nearTriangles = [E, C, c1]
                E.nearTriangles = [c2, D, F]

                # Y a los vecinos tambien les actualizamos las listas
                if c0 != None:
                    c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(uno, dos))] = C
                if c1 != None:
                    c1.nearTriangles[c1.giveMeIndex(c1.findOtherVertex(dos, tres))] = F
                if c2 != None:
                    c2.nearTriangles[c2.giveMeIndex(c2.findOtherVertex(tres, cero))] = E
                if c3 != None:
                    c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(cero, uno))] = D

                # Matamos el triangulo encontrado y el vecino, reemplazandolo
                # por None en la lista
                self.killTriangle(triangleFound)
                self.killTriangle(neighborTriangle)
                return None

        # Si es isInsideCollinearOrDuplicated es True, entonces el vertice
        # buscado estaba dentro de un triangulo, asi que hay que crear 3
        # triangulos nuevos sobre el encontrado.

        # Lo agrego a la lista
        self.vertexList.append(vertexToAdd)

        # 3 vertices a usar y tres vecinos
        cero = triangleFound.vertexAssociated[0]
        uno = triangleFound.vertexAssociated[1]
        dos = triangleFound.vertexAssociated[2]
        n0 = triangleFound.nearTriangles[2]
        n1 = triangleFound.nearTriangles[0]
        n2 =  triangleFound.nearTriangles[1]

        # Creamos los nuevos triangulos
        D = self.createTriangle([cero, uno, vertexToAdd])
        A = self.createTriangle([vertexToAdd, uno, dos])
        B = self.createTriangle([dos, cero, vertexToAdd])

        # Actualizamos la lista de triangulos vecinos de cada uno
        D.nearTriangles = [A, B, n0]
        A.nearTriangles = [n1, B, D]
        B.nearTriangles = [D, A, n2]

        # Y a los vecinos tambien se las actualizamos
        if n0 != None:
            n0.nearTriangles[n0.giveMeIndex(n0.findOtherVertex(cero, uno))] = D
        if n1 != None:
            n1.nearTriangles[n1.giveMeIndex(n1.findOtherVertex(uno, dos))] = A
        if n2 != None:
            n2.nearTriangles[n2.giveMeIndex(n2.findOtherVertex(dos, cero))] = B

        # Matamos el triangulo encontrado, reemplazandolo por None en la
        # lista de triangulos de la triangulacion
        self.killTriangle(triangleFound)
        return None

    # Este metodo dibuja la triangulacion
    def drawTriangulation(self):
        fig, axes = plt.subplots()
        axes.set(title="Bad Triangulation")
        fig.set_size_inches(18.5, 10.5)
        fig.set_dpi(120)
        # Dibujamos el triangulo creado
        for i in range(len(self.triangleList)):
            triangle = self.triangleList[i]
            if triangle == None:
                continue
            for j in range(4):
                v = triangle.vertexAssociated[j % 3]
                v1 = triangle.vertexAssociated[(j+1) % 3]
                plt.plot([v.x, v1.x], [v.y, v1.y], 'k')
        plt.savefig("badtriangulation.png")

    # Encuentro la arista compartida entre dos triangulos
    def findCommonEdge(self, triangleOne, triangleTwo):
        assert type(triangleOne) == Triangle
        assert type(triangleTwo) == Triangle

        # Asigno mis vertices a variables para que se clarifique el proceso
        cero = triangleOne.vertexAssociated[0]
        uno = triangleOne.vertexAssociated[1]
        dos = triangleOne.vertexAssociated[2]

        tres = triangleTwo.vertexAssociated[0]
        cuatro = triangleTwo.vertexAssociated[1]
        cinco = triangleTwo.vertexAssociated[2]

        # Creo mis aristas
        edgesOne = [Edge([cero, uno]), Edge([uno, dos]), Edge([dos, cero])]
        edgesTwo = [Edge([tres, cuatro]), Edge([cuatro, cinco]), Edge([cinco, tres])]

        for i in range(3):
            for j in range(3):
                if edgesTwo[j].isEqual(edgesOne[i]):
                    return edgesOne[i]

        # Si no tienen arista comun, retorno None, pero no deberia pasar puesto
        # que siempre voy a usar dos triangulos vecinos
        return None

    # Esta funcion reemplaza un triangulo en la lista triangleList por un None
    def killTriangle(self, triangle):
        self.triangleList[triangle.index] = None
        return None

    def legalizeEdge(self, indexNewVertex, edge, triangle):
        assert type(triangle) == Triangle
        assert type(edge) == Edge

        # Revisamos si la arista terminal es constrained (restringida)
        for i in range(len(self.edgesConstrained)):
            if edge.isEqual(self.edgesConstrained[i]):
                return None

        # Tomo el vecino para comprobar la legalidad de la arista
        neighbor = triangle.nearTriangles[indexNewVertex]
        if neighbor == None: return None

        # Vertice que se anhadio
        vertexAdded = triangle.vertexAssociated[indexNewVertex]

        # Vertices del edge a analizar
        edgeVertexOne = edge.vertices[0] # v0
        edgeVertexTwo = edge.vertices[1] # v1

        # Extraigo el vertice opuesto al edge comun entre triangle y neighbor
        indexOfVertexToAnalyze = neighbor.giveMeIndex(neighbor.findOtherVertex(edgeVertexOne, edgeVertexTwo))
        vertexToAnalyze = neighbor.vertexAssociated[indexOfVertexToAnalyze]

        # Compruebo que el vertice a analizar no este dentro del circuncirculo
        # de triangle
        if vertexToAnalyze.inCircle(triangle) or vertexAdded.inCircle(neighbor):
            # Con estos indices accederemos a los nuevos triangulos
            triangleIndex = triangle.index
            neighborIndex = neighbor.index
            # Aqui se realiza el flipEdge
            self.flipEdge(neighbor, triangle, edge, indexOfVertexToAnalyze, indexNewVertex)
            # Tomo los nuevos triangulos
            newTriangleOne = self.triangleList[triangle.index]
            newTriangleTwo = self.triangleList[neighbor.index]
            # Encuentro el indice del vertex que se anhadio (vertexAdded)
            indexVertexAddedTriangleOne = newTriangleOne.giveMeIndex(vertexAdded)
            indexVertexAddedTriangleTwo = newTriangleTwo.giveMeIndex(vertexAdded)
            # Los edge a revisar
            edgeOne = Edge([edgeVertexOne, vertexToAnalyze])
            edgeTwo = Edge([vertexToAnalyze, edgeVertexTwo])
            # Compruebo que los demas edges sean legales de igual forma
            self.legalizeEdge(indexVertexAddedTriangleOne, edgeOne, newTriangleOne)
            self.legalizeEdge(indexVertexAddedTriangleTwo, edgeTwo, newTriangleTwo)

        return None

    # Ingresa un punto de forma delaunay, self es la triangulacion, v es el
    # vertice a agregar, mode es el modo (si cayo en un triangulo o en una
    # arista), trueOrEdge es True si esta dentro del triangulo o una lista
    # con los indices de los vertices de la arista en la que esta el triangulo
    # y triangleFound es el triangulo en el que esta dentro si es que esta
    def delaunayInsertion(self, v, trueOrEdge, triangleFound):
        assert type(v) == Vertex
        assert type(triangleFound) == Triangle
        assert type(trueOrEdge) == list or trueOrEdge == True

        # Si esta contenido dentro del triangulo
        if trueOrEdge == True:
            # Lo agrego a la lista
            self.vertexList.append(v)

            # 3 vertices a usar y tres vecinos
            cero = triangleFound.vertexAssociated[0]
            uno = triangleFound.vertexAssociated[1]
            dos = triangleFound.vertexAssociated[2]
            n0 = triangleFound.nearTriangles[2]
            n1 = triangleFound.nearTriangles[0]
            n2 =  triangleFound.nearTriangles[1]

            # Creamos los nuevos triangulos
            D = self.createTriangle([cero, uno, v])
            A = self.createTriangle([v, uno, dos])
            B = self.createTriangle([dos, cero, v])

            # Actualizamos la lista de triangulos vecinos de cada uno
            D.nearTriangles = [A, B, n0]
            A.nearTriangles = [n1, B, D]
            B.nearTriangles = [D, A, n2]

            # Y a los vecinos tambien se las actualizamos
            if n0 != None:
                n0.nearTriangles[n0.giveMeIndex(n0.findOtherVertex(cero, uno))] = D
            if n1 != None:
                n1.nearTriangles[n1.giveMeIndex(n1.findOtherVertex(uno, dos))] = A
            if n2 != None:
                n2.nearTriangles[n2.giveMeIndex(n2.findOtherVertex(dos, cero))] = B

            # Matamos el triangulo encontrado, reemplazandolo por None en la
            # lista de triangulos de la triangulacion
            self.killTriangle(triangleFound)

            # Ahora hay que revisar que estos triangulos nuevos tengan todos
            # aristas legales
            # Estas seran las aristas a revisar
            edgeOne = Edge([cero, uno]) # Triangulo D
            edgeTwo = Edge([uno, dos]) # Triangulo A
            edgeThree = Edge([dos, cero]) # Triangulo B

            # Compruebo la legalidad
            self.legalizeEdge(D.giveMeIndex(v), edgeOne, D)
            self.legalizeEdge(A.giveMeIndex(v), edgeTwo, A)
            self.legalizeEdge(B.giveMeIndex(v), edgeThree, B)

        # Si esta sobre una arista
        else:
            # Lo agrego a la lista
            self.vertexList.append(v)

            # Una expresion matematica para calcular el indice del vertice restante
            # de un triangulo dados los otros dos indices de vertices
            indexFaltante = (3 -((trueOrEdge[1] +
                trueOrEdge[0]) % 3)) % 3

            # 3 vertices a usar
            cero = triangleFound.vertexAssociated[trueOrEdge[1]]
            uno = triangleFound.vertexAssociated[indexFaltante]
            dos = triangleFound.vertexAssociated[trueOrEdge[0]]

            # Triangulo vecino
            neighborTriangle = triangleFound.nearTriangles[indexFaltante]

            # Necesitamos el 4to vertice a usar que esta en el triangulo
            # contrario al vertice indexFaltante
            tres = neighborTriangle.findOtherVertex(cero, dos)

            # Creamos los triangulos nuevos
            D = self.createTriangle([cero, uno, v])
            C = self.createTriangle([v, uno, dos])
            F = self.createTriangle([dos, tres, v])
            E = self.createTriangle([v, tres, cero])

            # Triangulos vecinos
            c0 = triangleFound.nearTriangles[triangleFound.giveMeIndex(cero)]
            c1 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(cero)]
            c2 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(dos)]
            c3 = triangleFound.nearTriangles[triangleFound.giveMeIndex(dos)]

            # Les actualizamos la lista de triangulos vecinos
            D.nearTriangles = [C, E, c3]
            C.nearTriangles = [c0, F, D]
            F.nearTriangles = [E, C, c1]
            E.nearTriangles = [c2, D, F]

            # Y a los vecinos tambien les actualizamos las listas
            if c0 != None:
                c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(uno, dos))] = C
            if c1 != None:
                c1.nearTriangles[c1.giveMeIndex(c1.findOtherVertex(dos, tres))] = F
            if c2 != None:
                c2.nearTriangles[c2.giveMeIndex(c2.findOtherVertex(tres, cero))] = E
            if c3 != None:
                c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(cero, uno))] = D

            # Matamos el triangulo encontrado y el vecino, reemplazandolo
            # por None en la lista
            self.killTriangle(triangleFound)
            self.killTriangle(neighborTriangle)

            # Ahora hay que revisar que estos triángulos nuevos tengan todos
            # aristas legales
            edgeOne = Edge([cero, uno]) # Triangulo D
            edgeTwo = Edge([uno, dos]) # Triangulo C
            edgeThree = Edge([dos, tres]) # Triangulo F
            edgeFour = Edge([tres, cero]) # Triangulo E

            # Compruebo la legalidad
            self.legalizeEdge(D.giveMeIndex(v), edgeOne, D)
            self.legalizeEdge(C.giveMeIndex(v), edgeTwo, C)
            self.legalizeEdge(F.giveMeIndex(v), edgeThree, F)
            self.legalizeEdge(E.giveMeIndex(v), edgeFour, E)


    # Calcula el centroide del cuadrilatero y lo ingresa de forma delaunay
    def centroidInsertion(self, t1, t2):
        assert type(t1) == Triangle
        assert type(t2) == Triangle

        # Calculamos el area de cada uno de estos Triangulos
        area_t1 = t1.area()
        area_t2 = t2.area()
        total_area = area_t1 + area_t2

        # Ademas calculamos el centroide de cada uno de estos triángulos
        # Hay que tomar en cuenta que estos son numpy arrays
        centroid_t1 = t1.centroid()
        centroid_t2 = t2.centroid()

        # Sumamos los centroides y tenemos el centroide del cuadrilatero
        centroid = ((area_t1 * centroid_t1) + (area_t2 * centroid_t2)) / total_area

        # Transformo en un Vertex Object
        centroid_V = Vertex(centroid[0], centroid[1])

        # Buscar en cual de los dos triangulos cayo el punto y si esta dentro
        # o encima de la arista que los une.
        # trueOrEdge: True or [index1, index2]
        trueOrEdge, triangleFound = centroid_V.searchVertexInTwoTriangles(t1, t2)

        # Lo insertamos Delaunay
        self.delaunayInsertion(centroid_V, trueOrEdge, triangleFound)

        return None

    # Bisecciona los dos triangulos terminales si la arista terminal es
    # restringida: indexV1 es el indice opuesto a edge de triangle
    def bisection(self, indexV1, triangle, edge):
        assert type(triangle) == Triangle
        assert type(edge) == Edge

        # Calculo el punto medio de la arista
        middlePoint = edge.middle()

        # Lo agrego a la lista
        self.vertexList.append(middlePoint)

        # 3 vertices a usar
        cero = triangle.vertexAssociated[triangle.giveMeIndex(edge.vertices[1])]
        uno = triangle.vertexAssociated[indexV1]
        dos = triangle.vertexAssociated[triangle.giveMeIndex(edge.vertices[0])]

        # Triangulo vecino
        neighborTriangle = triangle.nearTriangles[indexV1]

        # Si el vecino es None entonces realizamos solo una biseccion para
        # el triangulo que fue pasado como parametro - Caso especial.
        if neighborTriangle == None:
            # Creamos los triangulos nuevos
            D = self.createTriangle([cero, uno, middlePoint])
            C = self.createTriangle([middlePoint, uno, dos])

            # Triangulos vecinos
            c0 = triangle.nearTriangles[triangle.giveMeIndex(cero)]
            c3 = triangle.nearTriangles[triangle.giveMeIndex(dos)]

            # Actualizamos la lista de los triangulos nuevos
            D.nearTriangles = [C, None, c3]
            C.nearTriangles = [c0, None, D]

            # A los vecinos tambien les actualizamos la lista
            if c0 != None:
                c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(uno, dos))] = C
            if c3 != None:
                c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(cero, uno))] = D

            # Marcamos el triangulo que biseccionamos
            self.killTriangle(triangle)

            return None

        # Necesitamos el 4to vertice a usar que esta en el triangulo
        # contrario al vertice indexFaltante
        tres = neighborTriangle.findOtherVertex(cero, dos)

        # Creamos los triangulos nuemiddlePointos
        D = self.createTriangle([cero, uno, middlePoint])
        C = self.createTriangle([middlePoint, uno, dos])
        F = self.createTriangle([dos, tres, middlePoint])
        E = self.createTriangle([middlePoint, tres, cero])

        # Triangulos vecinos
        c0 = triangle.nearTriangles[triangle.giveMeIndex(cero)]
        c1 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(cero)]
        c2 = neighborTriangle.nearTriangles[neighborTriangle.giveMeIndex(dos)]
        c3 = triangle.nearTriangles[triangle.giveMeIndex(dos)]

        # Les actualizamos la lista de triangulos vecinos
        D.nearTriangles = [C, E, c3]
        C.nearTriangles = [c0, F, D]
        F.nearTriangles = [E, C, c1]
        E.nearTriangles = [c2, D, F]

        # Y a los vecinos tambien les actualizamos las listas
        if c0 != None:
            c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(uno, dos))] = C
        if c1 != None:
            c1.nearTriangles[c1.giveMeIndex(c1.findOtherVertex(dos, tres))] = F
        if c2 != None:
            c2.nearTriangles[c2.giveMeIndex(c2.findOtherVertex(tres, cero))] = E
        if c3 != None:
            c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(cero, uno))] = D

        # Matamos el triangulo encontrado y el vecino, reemplazandolo
        # por None en la lista
        self.killTriangle(triangle)
        self.killTriangle(neighborTriangle)

        return None

    # flipEdge simple
    def flipEdge(self, triangleOne, triangleTwo, commonEdge, indexV1, indexV3):
        assert type(triangleOne) == Triangle
        assert type(triangleTwo) == Triangle

        # Separo la arista en mis dos vertices
        v0 = commonEdge.vertices[0]
        v2 = commonEdge.vertices[1]

        # Extraigo los vertices restantes
        v1 = triangleOne.vertexAssociated[indexV1]
        v3 = triangleTwo.vertexAssociated[indexV3]
        # Tomo los triangulos vecinos de cada uno
        c0 = triangleOne.nearTriangles[triangleOne.giveMeIndex(v2)]
        c1 = triangleOne.nearTriangles[triangleOne.giveMeIndex(v0)]
        c2 = triangleTwo.nearTriangles[triangleTwo.giveMeIndex(v0)]
        c3 = triangleTwo.nearTriangles[triangleTwo.giveMeIndex(v2)]

        # Creo mis nuevos triangulos
        newTriangleOne = Triangle([v1, v2, v3])
        newTriangleTwo = Triangle([v3, v0, v1])

        # Actualizo los vecinos de los triangulos nuevos
        newTriangleOne.nearTriangles = [c2, newTriangleTwo, c1]
        newTriangleTwo.nearTriangles = [c0, newTriangleOne, c3]

        # Actualizo los nuevos vecinos de los triangulos vecinos
        if c0 != None:
            c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(v0, v1))] = newTriangleTwo
        if c1 != None:
            c1.nearTriangles[c1.giveMeIndex(c1.findOtherVertex(v1, v2))] = newTriangleOne
        if c2 != None:
            c2.nearTriangles[c2.giveMeIndex(c2.findOtherVertex(v2, v3))] = newTriangleOne
        if c3 != None:
            c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(v3, v0))] = newTriangleTwo

        # Actualizo los indices de los triangulos en el arreglo de triangulos
        # de la triangulacion
        newTriangleOne.index = triangleOne.index
        newTriangleTwo.index = triangleTwo.index

        # Reemplazo los triangulos viejos por los nuevos
        self.triangleList[newTriangleOne.index] = newTriangleOne
        self.triangleList[newTriangleTwo.index] = newTriangleTwo

        return None

    # Hace la recursion de lepp
    def lepp(self, t):
        assert type(t) == Triangle

        # Encuentro el longestEdge de t y el indice opuesto a el
        longestEdge, oppositeIndex = t.longestEdge()

        # Encuentro asi su vecino
        neighbor = t.nearTriangles[oppositeIndex]

        # Encuentro el longestEdge del vecino de t y el indice opuesto a ese
        if neighbor != None:
            neighborLongestEdge, neighborOppositeIndex = neighbor.longestEdge()

            # Si el vecino por parte de la arista mas larga de neighbor es t
            # entonces hemos encontrado la arista y los triangulos terminales
            if neighbor.nearTriangles[neighborOppositeIndex] == t:
                vt = t.vertexAssociated[oppositeIndex]
                vn = neighbor.vertexAssociated[neighborOppositeIndex]
                circleOfT = vn.inCircle(t)
                circleOfNeighbor = vt.inCircle(neighbor)

                constrained = False
                # Revisamos si la arista terminal es constrained (restringida)
                for i in range(len(self.edgesConstrained)):
                    if longestEdge.isEqual(self.edgesConstrained[i]):
                        constrained = True
                        break

                # Si la arista terminal no es renstringida
                if not constrained:
                    # Si no son delaunay hago flip simple
                    if circleOfT:
                        return self.flipEdge(neighbor, t, longestEdge, neighborOppositeIndex, oppositeIndex)
                    elif circleOfNeighbor:
                        return self.flipEdge(t, neighbor, longestEdge, oppositeIndex, neighborOppositeIndex)
                    # Si son delaunay ingreso el centroide
                    else:
                        return self.centroidInsertion(t, neighbor)
                # Si la arista es restringida entonces aplico la biseccion de forma normal
                else:
                    return self.bisection(oppositeIndex, t, longestEdge)
        else: # Aplicar biseccion de t
            return self.bisection(oppositeIndex, t, longestEdge)

        # Caso recursivo, avanzamos al triangulo neighbor
        return self.lepp(neighbor)

    # Funcion que me recorre la triangulacion y procesa todo triangulo malo que
    # vea
    def processTriangulation(self):
        self.badTriangles = 0
        for i in range(len(self.triangleList)):
            print("Refinando triangulo " + str(i))

            # Vemos si el triangulo es malo y si es malo, le aplicamos lepp
            if self.triangleList[i] != None and self.triangleList[i].isBad():
                self.badTriangles += 1
                # Mientras que t permaneza en la triangulacion o sea malo,
                # seguimos aplicando Lepp(t)
                j = 0
                while self.triangleList[i].isBad():
                    self.lepp(self.triangleList[i])
                    # Si en algun momento el triangulo se transforma en None,
                    # me salgo del ciclo puesto que ya desaparecio
                    if self.triangleList[i] == None: break
                    j += 1

                # Si se quiere dibujar los pasos, descomentar esto
                self.drawRefinedTriangulation(i)

    # Dibuja la triangulacion
    def drawRefinedTriangulation(self, z = None):
        fig, axes = plt.subplots()
        if z == None:
            axes.set(title=f"Final refined triangulation", xticks=[], yticks=[])
        else:
            axes.set(title=f"Refined triangulation {z}", xticks=[], yticks=[])
        fig.set_size_inches(18.5, 10.5)
        fig.set_dpi(120)
        # Dibujamos el triangulo creado
        for i in range(len(self.triangleList)):
            triangle = self.triangleList[i]
            if triangle == None:
                continue
            for j in range(4):
                v = triangle.vertexAssociated[j % 3]
                v1 = triangle.vertexAssociated[(j+1) % 3]
                plt.plot([v.x, v1.x], [v.y, v1.y], 'k')
        if z == None:
            plt.savefig(f'ref_final.png')
        else:
            plt.savefig(f'ref{z}.png')

    # printea los vertices para reusarlos en un archivo y probar nuevas estrat.
    def printVertices(self):
        print("\nVertices:\n")
        for i in range(len(self.vertexList)):
            print(str(self.vertexList[i].x) + "," + str(self.vertexList[i].y))

# Main body del programa
if __name__ == '__main__':

    # Funcion recursiva de agregar puntos
    def addPoints():
        p = input("\nPunto: ")
        comaIndex = p.find(",")
        if p == "end":
            print("\nDisfrute sus triangulaciones! Bye bye nwn.")
            return None
        v = Vertex(int(p[0:comaIndex]), int(p[comaIndex + 1:len(p)]))
        T.addVertex(v)
        print("Punto ingresado correctamente.")
        return addPoints()

    # Pedimos prompt al usuario
    n = input("Ingrese un numero de ptos. aleatorios: ")
    print("\nSi va a ingresar una triangulacion restringida por favor " +
        "multiplicar su limite escogido por 5 para escalarla correctamente.")
    nplus = input("Ingrese limite superior: ")

    # Inicializamos la triangulacion
    T = Triangulation(int(nplus))
    T.interactive = input("Quiere que el dibujo sea interactivo (y/n)?: ")
    # Si quiere...
    if T.interactive == 'y':
        # Muestra la pantalla interactiva
        plt.show()
        ax = plt.gca()
    # Si no es ni n ni y, entonces...
    elif T.interactive != 'n':
        print("Solo se admiten respuestas como \"y\" y \"n\", " +
            "por favor ejecute nuevamente el programa.")
        exit()

    # Si no queremos anhadir ptos aleatorios
    if int(n) == 0:
        p = input("\nPunto: ")
        comaIndex = p.find(",")
        v = Vertex(int(p[0:comaIndex]), int(p[comaIndex + 1:len(p)]))
        T.vertexList.append(v)
        # Dibujamos la primera parte de la triangulacion.
        T.createTriangulation(T.vertexList)
        print("Punto ingresado correctamente.")
        addPoints()

    # Si anhadimos ptos. aleatorios
    elif int(n) > 0:
        # Creo numeros aleatorios
        verticesMatrix = np.random.randint(0, int(nplus), size=(int(n), 2))

        # Creamos la lista de vertices usando la matriz anterior
        for i in range(int(n)):
            v = Vertex(verticesMatrix[i][0], verticesMatrix[i][1])
            T.vertexList.append(v)
        # Inicializamos la triangulacion
        T.createTriangulation(T.vertexList)
        # Si queremos anhadir mas puntos
        addPoints()

    # Para ingresar una serie de puntos que estan en un archivo csv
    elif int(n) == -1:
        # Ingresamos el nombre del archivo
        fileName = input("Ingrese nombre de archivo de puntos: ")
        # Abrimos y leemos
        with open(fileName + ".csv", "r") as file:
            reader = csv.reader(file)
            # Leemos e ingresamos valores de vertices
            for row in reader:
                T.vertexList.append(Vertex(int(row[0]), int(row[1])))
        # Inicializamos triangulacion
        T.createTriangulation(T.vertexList)
        # Si queremos anhadir mas puntos
        addPoints()

    # Si vamos a anhadir un set de puntos con aristas restringidas
    # Esto con cualquier numero menor a -1
    else:
        # Ingresamos el nombre del archivo
        fileName = input("Ingrese nombre de archivo con aristas restringidas: ")
        # Abrimos y leemos
        with open(fileName + ".csv", "r") as file:
            reader = csv.reader(file)
            # Leemos e ingresamos edges a la lista de edges restringidos de T
            # El i es una var auxiliar que nos ayudara a elegir los vertices de
            # la arista restringida sin tener que volverlo a tomar del archivo
            # El "* 5" es para escalar la triangulacion
            i = 0
            for row in reader:
                T.vertexList.append(Vertex(int(row[0]) * 5, int(row[1]) * 5))
                T.vertexList.append(Vertex(int(row[2]) * 5, int(row[3]) * 5))
                T.edgesConstrained.append(Edge([T.vertexList[i], T.vertexList[i + 1]]))
                i += 2
        # Inicializamos la triangulacion
        T.createTriangulation(T.vertexList)
        # Si queremos anhadir mas puntos
        addPoints()

    # Inicializamos la triangulacion
    T.printTriangles()
    T.drawTriangulation()
    T.printVertices()

    print("\nAhora veremos la refinacion de la triangulacion.\n")

    i = 1
    while T.badTriangles != 0:
        T.processTriangulation()
        T.drawRefinedTriangulation(i)
        print("Triangulos malos encontrados durante el proceso: " + str(T.badTriangles))
        i += 1
